function OddEven(num){
	num % 2 == 0 ? console.log(`the number ${num} is even`) : (num % 2 == 1 ? console.log(`the number ${num} is odd`) : console.log("invalid input"));
}

function budgetChecker(num){
	num > 40000 ? console.log("you are over budget") : (num <= 40000 ? console.log("you have resources left") : console.log("invalid input"))
}

OddEven(60);
OddEven(32);
OddEven(671);

budgetChecker(42000);
budgetChecker(2300);